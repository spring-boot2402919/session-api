package com.enkeapple.session.component;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class RequestContextFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            RequestContextHolder.setRequestAttributes(new ServletRequestAttributes((HttpServletRequest) request));

            chain.doFilter(request, response);
        } finally {
            RequestContextHolder.resetRequestAttributes();
        }
    }
}
