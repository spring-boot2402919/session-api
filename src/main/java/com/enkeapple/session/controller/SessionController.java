package com.enkeapple.session.controller;

import com.enkeapple.session.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/session")
public class SessionController {
    final SessionService sessionService;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @GetMapping("/session-id")
    public ResponseEntity<String> getSessionId() {
        return ResponseEntity.ok(sessionService.getSessionId());
    }

    @GetMapping("/session-id-async")
    public CompletableFuture<ResponseEntity<String>> getSessionIdAsync() {
        return sessionService.getSessionIdAsync().thenApply(ResponseEntity::ok);
    }
}
